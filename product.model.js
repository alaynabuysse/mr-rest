var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({
  name: String,
  id: Number,
  current_price: { value: Number, currency_code: String}
});

var Product = mongoose.model('products', productSchema);

module.exports = Product;
