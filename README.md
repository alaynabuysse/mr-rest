# RESTful App
## Getting Started
### Hosted Version
There is a hosted version of the application [here](http://35.223.54.187:3004/products/13860428). 
### Setting up the Database
If you'd like to run this application locally, you will have to start by setting up the database. This application uses MongoDB.

**Getting MongoDB Installed**
1. You can follow [these installation instructions](https://docs.mongodb.com/manual/installation/) from the MongoDB website to get started.
2. To make things easier, I also used [Robo 3T](https://robomongo.org/). This made it easier to create the database.

**Setting up the Database**
1. Once you have Robo 3T installed, open the application.
2. Connect to your localhost database
3. Right click `New Connection` and click `Create Database`
4. Make the name of the database `productsDB` and click `Create`

**Import Data**
In this project there's a mongodump file called `dump/productsDB/products.bson`. To import this data into your database:
1. Open a terminal window
2. Clone this project by running `git clone https://gitlab.com/alaynabuysse/mr-rest.git`
3. Run `cd mr-REST`
4. Run `mongorestore -h localhost:27017 -d productsDB -u username -p password dump/productsDB/`
5. In order for the application to connect to this new database, you will have to change the connection string in the `app.js` file from `mongodb://10.128.0.3:27017/productsDB` to `mongodb://localhost:27017/productsDB`

### Starting the Application
Before getting started, you will want to make sure you have [Node.js](https://nodejs.org/en/download/) already installed. To run the application locally:
1. Open a terminal window and`cd` into the folder that is holding the project.
2. Run `npm install`
3. Run `npm start`
4. The application is now running at `localhost:3004`!

### Running Tests
Once you have everything running, you can run the tests by:
1. Openning a terminal window
2. `cd` into the project
3. Run `npm install mocha chai` (You will only ever have to run this step once)
4. Run `mocha test`

## Interacting with the application
Below are the following APIs and instructions on how to use them.

### GET /products/{id}
Add a product ID to the end of the URL and get back information on the product from the database. You can use your browser to test this API.

**Example Input**
```
http://localhost:3004/products/13860428
```

**Expected Response**
```
{"current_price":{"value":13.49,"currency_code":"USD"},"_id":"5e6ab0d2ad591b1a916d1ca2","id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","__v":0}
```

**Valid Input IDs from MongoDB Import**
* 13860428
* 15117729
* 16483589
* 16696652
* 16752456
* 15643793

### GET /official/products/{id}
Add a product ID to the end of the URL and get back product information from `redsky.target.com`. You can use your browser to test this API.

**Example Input**
```
http://localhost:3004/official/products/13860428
```

**Expected Response**
```
{"id":"13860428","name":"The Big Lebowski (Blu-ray)","current_price":{"value":13.49,"currency_code":"USD"}}
```

### POST /products
Input information about the product into the database. *NOTE:* For easier testing use [Postman](https://www.postman.com/).

**Example Input**

URL: localhost:3004/products

BODY:
```
{
	"id": 12345678,
	"name": "Best Shirt Ever (Seriously!!)",
	"current_price": {
		"value": 100.00,
		"currency_code": "USD"
	}
}
```

**Expected Response**
```
{
  "success": "New product created"
}
```

### PUT /products/:id
Update information about the product in the database. 

**Example Input**

URL: localhost:3004/products/12345678

BODY:
```
{
	"id": 12345678,
	"name": "Best Shirt Ever (Seriously!!)",
	"current_price": {
		"value": 1000000000.00,
		"currency_code": "USD"
	}
}
```

**Expected Response**
```
{
  "success": "Product 12345678 has been updated"
}
```


### DELETE /products/:id
Delete a product from the database.

**Example Input**
```
http://localhost:3004/official/products/12345678
```

**Expected Response**
```
{
  "success": "Product 12345678 deleted"
}
```

## Pipeline
There are three main sections in the pipeline for this project.

1. **Build:** The build stage builds a new docker image based on the changes commited to the GitLab project.
2. **Test:** The test stage starts up the container and runs mocha tests.
3. **Destroy:** The destroy stage only runs if either of the previous stages fails. This way, the "bad image" won't be used.


## Questions?
If you have a question or don't think the application is working as intended, [create a new issue](https://gitlab.com/alaynabuysse/mr-rest/-/issues/new) on this project!