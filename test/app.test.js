const expect = require('chai').expect;
const request = require('request');
let url = "http://localhost:3004"


describe('Get product information', function(){
  it('Get product information from database',function(done){
    request(url + "/products/13860428",function(err, res, body){
      let expectedResponse = {
          current_price:
            {
              value: 13.49,
              currency_code: "USD"
            },
            _id: "5e6ab0d2ad591b1a916d1ca2",
            id: 13860428,
            name: "The Big Lebowski (Blu-ray) (Widescreen)",
            __v: 0
        }
      body = JSON.parse(body);
      expect(body.current_price.value).to.equal(expectedResponse.current_price.value);
      expect(body.current_price.currency_code).to.equal(expectedResponse.current_price.currency_code);
      expect(body.id).to.equal(expectedResponse.id);
      expect(body.name).to.equal(expectedResponse.name);
      expect(res.statusCode).to.equal(200);
      done();
    });
  });

  it('Get product information from API and database',function(done){
    request(url + '/official/products/13860428',function(err, res, body){
      let expectedResponse = {
          id: "13860428",
          name: "The Big Lebowski (Blu-ray)",
          current_price: {
              value: 13.49,
              currency_code: "USD"
            }
        }
        body = JSON.parse(body);
        expect(body.current_price.value).to.equal(expectedResponse.current_price.value);
        expect(body.current_price.currency_code).to.equal(expectedResponse.current_price.currency_code);
        expect(body.id).to.equal(expectedResponse.id);
        expect(body.name).to.equal(expectedResponse.name);
        expect(res.statusCode).to.equal(200);
      done();
    });
  });
});

describe('Create new product', function(){
    it('Input new product',function(done){
      let newProduct = {
        current_price:
          {
            value: 13.49,
            currency_code: "USD"
          },
          _id: "5e6ab0d2ad591b1a916d1ca2",
          id: 13860430,
          name: "The Big Lebowski (Blu-ray) (Widescreen)",
          __v: 0
      }
      request.post({url: url + "/products", form: newProduct},function(err, res, body){
        body = JSON.parse(body);
        expect(body.success).to.equal("New product created");
        expect(res.statusCode).to.equal(201);
        done();
      });
    });
});

describe('Update existing product', function(){
    it('Update product price',function(done){
      let updateProduct = {
        current_price:
          {
            value: 15.49,
            currency_code: "USD"
          },
          _id: "5e6ab0d2ad591b1a916d1ca2",
          id: 13860430,
          name: "The Big Lebowski (Blu-ray) (Widescreen)",
          __v: 0
      }
      request.put({url: url + "/products/13860430", form: updateProduct},function(err, res, body){
        body = JSON.parse(body);
        expect(body.success).to.equal("Product 13860430 has been updated");
        expect(res.statusCode).to.equal(200);
        done();
      });
    });
});

describe('Delete product', function(){
    it('Delete product',function(done){
      request.delete(url + "/products/13860430",function(err, res, body){
        body = JSON.parse(body);
        expect(body.success).to.equal("Product 13860430 deleted");
        expect(res.statusCode).to.equal(200);
        done();
      });
    });
});