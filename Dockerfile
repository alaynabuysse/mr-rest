FROM node:latest

ADD . /

COPY package* /

RUN npm install

EXPOSE 3004

CMD ["npm","start"]