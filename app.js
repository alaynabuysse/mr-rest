const express = require('express');
const mongoose = require('mongoose');
const Product = require('./product.model');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();
app.use(bodyParser.json());

mongoose.connect('mongodb://10.128.0.3:27017/productsDB');

// POST - Create new product in database
app.post('/products', function(req, res){
  let newProduct = new Product(req.body);

  newProduct.save(function(err){
    if(err){
      console.log(err);
      let errorResponse = {
        error: "Unable to save product in database."
      };
      res.status(500).send(errorResponse);
    }
    else{
      let finalResponse = {
        success: "New product created"
      };
      res.status(201).send(finalResponse);
    }
  });
});

// GET - Use ID to get product information from database
app.get('/products/:id',function(req,res){
  Product.find({id: req.params.id},function(err, results){
    if(err){
      console.log(err);
      let errorResponse = {
        error: "Error finding product with ID " + req.query.id
      };
      res.status(500).send(errorResponse);
    }
    else{
      res.send(results[0]).status(200);
    }
  });
});

// GET - Use ID to get product info from API and then getting rest of info from database
app.get('/official/products/:id',function(req,res){
    let url = "http://redsky.target.com/v2/pdp/tcin/" + req.params.id + "?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics";
    request(url,function(error,response,body){
        if(error){
            console.log(error);
            let errorResponse = {
                error: "Error finding product."
            };
            res.status(500).send(errorResponse);
        }
        else{
            body = JSON.parse(body);
            if("product_description" in body.product.item){
                let productName = body.product.item.product_description.title;
                Product.find({id: req.params.id},function(err, results){
                    if(err){
                      console.log(err);
                      let errorResponse = {
                        error: "Error finding product in database"
                      };
                      res.status(500).send(errorResponse);
                    }
                    else{
                      let finalResponse = {
                          id: req.params.id,
                          name: productName,
                          current_price: results[0].current_price
                      };
                      res.send(finalResponse).status(200);
                    }
                  });
            }
            else{
                let errorResponse = {
                    error: "Product with ID " + req.params.id + " not found."
                };
                res.status(404).send(errorResponse);
            }
        }
    });
});

// PUT - Update price info of product
app.put('/products/:id',function(req,res){
    Product.findOneAndUpdate({id: req.params.id},req.body,function(err,results){
        if(err){
            console.log(err);
            let errorResponse = {
                error: "Error updating product"
            };
            res.status(500).send(errorResponse);
        }
        else{
            let finalResponse = {
                success: "Product " + req.params.id + " has been updated"
            };
            res.send(finalResponse).status(200);
        }
    });
});


// DELETE - Delete product entry (mainly used for testing)
app.delete('/products/:id',function(req,res){
    Product.remove({
        id: req.params.id
      },function(err, product){
        if(err){
          console.log(err);
          let errorResponse = {
            error: "Unable to delete product entry"
          };
          res.send(errorResponse).status(500);
        }
        else{
            let finalResponse = {
                success: "Product " + req.params.id + " deleted"
            };
          res.status(200).send(finalResponse);
        }
      });
});

app.listen(3004, function(){
  console.log('Server up on 3004!!');
});
